from django.urls import path
from .views import index


appname = 'homepage'

urlpatterns = [
   path('', index, name = 'bookList'),
]
