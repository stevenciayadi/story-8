from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import index

# Create your tests here.
class story8(TestCase):

    def test_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)
    
    def test_page_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,'index.html')
    
    def test_json_url(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Daftar Buku",html_response)

class FuncionalTest8(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/')
    
    def tearDown(self):
        self.selenium.quit()
    
    def test_open_page(self):
        browser = self.selenium
        self.assertIn("Daftar Buku",browser.title)
    
    def test_header_page(self):
        browser = self.selenium
        self.assertIn("Daftar Buku",browser.page_source)
